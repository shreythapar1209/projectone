#!/bin/bash

echo "call to deploy script file"
mvn package spring-boot:repackage -DskipTests
if [ $? -eq 1 ]
then
  echo "mvn failed"
  exit 1
fi
docker image rm phx.ocir.io/axqel8fpeyhe/sys-data-product:latest
docker build --no-cache --progress=plain -t phx.ocir.io/axqel8fpeyhe/sys-data-product:latest .
docker push phx.ocir.io/axqel8fpeyhe/sys-data-product:latest
kubectl delete -f sys-data-product-dev-us.yml
kubectl apply -f sys-data-product-dev-us.yml
