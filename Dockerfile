FROM openjdk:11
EXPOSE 30011
ADD target/a10-saas-products-0.0.1-SNAPSHOT.jar a10-saas-products-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar", "a10-saas-products-0.0.1-SNAPSHOT.jar"]