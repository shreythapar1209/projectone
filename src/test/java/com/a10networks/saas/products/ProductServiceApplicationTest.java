package com.a10networks.saas.products;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author ssingh
 * @email ssingh2@a10networks.com
 */

@SpringBootTest
class ProductServiceApplicationTest {
	@Mock
	ProductServiceApplication productApplication;
	@Test
	void constructor() {
		assertNotNull(new ProductServiceApplication());
	}
}
