package com.a10networks.saas.products.repository;

import com.a10networks.saas.products.ProductServiceApplication;
import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.entity.Server;
import com.a10networks.saas.products.entity.Tenant;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;

/**
 * @author SSingh
 * @email ssingh2@a10networks.com
 */

@SpringBootTest(classes = ProductServiceApplication.class)
@ExtendWith(SpringExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_CLASS)
public class ProductRepositoryTest {
	private final ProductRepository productRepository;
	private Product product1, product2, product3;
	private List<Product> productList;

	private Tenant tenant;

	private Server server, server1;

	private Set<Product> productSaasDatabase;

	@Autowired
	public ProductRepositoryTest(ProductRepository productRepository) {
		this.productRepository = productRepository;
	}

	@BeforeEach
	public void setUp() {
		product3 = new Product(1,"product1");
		product1 = new Product(2,"product");
		product2 = new Product(3,"product1");
		tenant = new Tenant();
		tenant.setTenantName("tenant");
		tenant.setCompanyName("tenant");
		productSaasDatabase = new HashSet<>();
		server = new Server("saasDatabase","type","url","user","password","driver");
		server1 = new Server("saasDatabase1","type","url","user","password", "driver");
	}

	@AfterEach
	public void tearDown() {
		productRepository.deleteAll();
	}

	@Test
	public void test_save() {
		final Product product_response = productRepository.save(product1);
		assertEquals(product1.getProductName(), product_response.getProductName());
		assertNotNull(product_response.getProductId());
	}

	@Test
	public void test_save_inValidObject() {
		Product a = new Product();
		assertThrows(ConstraintViolationException.class, () -> productRepository.save(a));
	}
	@Test
	public void test_save_same_Product() {
		Product productEntity = productRepository.save(product1);
		Product Product_new = new Product();
		Product_new.setProductName("product");
		assertThrows(DataIntegrityViolationException.class,() -> productRepository.save(Product_new));
	}



	@Test
	public void test_findAll() {
		productRepository.save(product1);
		productRepository.save(product2);
		productList = (List<Product>) productRepository.findAll();
		assertNotNull(productRepository.findAll());
		assertThat(productList).size().isEqualTo(2);
	}

	@Test
	public void test_emptyTable_findAll() {
		productRepository.save(product1);
		productRepository.save(product2);
		productRepository.deleteAll();
		productList = (List<Product>) productRepository.findAll();
		assertNotNull(productRepository.findAll());
		assertThat(productList).size().isEqualTo(0);
	}

	@Test
	public void test_findById() {
		Product a = productRepository.save(product1);
		Product actual = productRepository.findById(a.getProductId()).get();
		assertEquals(a.getProductId(), actual.getProductId());
	}

	@Test
	public void test_findById_noSuchElementException() {
		Product a = productRepository.save(product1);
		productRepository.deleteAll();
		assertThrows(NoSuchElementException.class, () -> productRepository.findById(a.getProductId()).get());
	}

	@Test
	public void test_update() {
		Product p = productRepository.save(product1);
		Product productActual = productRepository.findById(p.getProductId()).get();
		productActual.setProductName("HC");
		productActual = productRepository.save(productActual);
		Assertions.assertNotNull(productActual.getProductId());
		Assertions.assertNotEquals(p.getProductName(), productActual.getProductName());
		assertEquals(p.getProductId(), productActual.getProductId());
	}
	@Test
	public void test_update_SameProduct(){
		Product productEntity= productRepository.save(product1);
		Product productEntity1= productRepository.findById(productEntity.getProductId()).get();
		Product productnew = new Product();
		productnew.setProductName(productEntity1.getProductName());
		assertThrows(DataIntegrityViolationException.class,() -> productRepository.save(productnew));
	}

	@Test
	public void test_deleteById() {
		Product a = productRepository.save(product1);
		productRepository.deleteById(a.getProductId());
		assertThrows(NoSuchElementException.class, () -> productRepository.findById(a.getProductId()).get());
	}

	@Test
	public void test_wrong_deleteById() {
		assertThrows(EmptyResultDataAccessException.class, () -> productRepository.deleteById(123));
	}

	@Test
	@Transactional
	public void test_findProductsByTenantsId() {
		tenant.addProduct(product1);
		tenant.addProduct(product2);
		Product product = productRepository.save(product1);
		List<Product> productRes = productRepository.findProductsByTenantsTenantId(product.getTenants().iterator().next().getTenantId());
		//assertEquals(productRes.get(0).getTenants().iterator().next().getName(),tenant.getName());
		assertEquals(productRes.get(0).getTenants().size(),1);
		assertEquals(productRes.size(),2);
	}

	@Test
	@Transactional
	public void test_findProductsByTenantsId_get_empty_list() {
		tenant.addProduct(product1);
		tenant.addProduct(product2);
		Product product = productRepository.save(product1);
		List<Product> productRes = productRepository.findProductsByTenantsTenantId(product.getTenants().iterator().next().getTenantId()+1);
		assertEquals(productRes.size(),0);
	}

	@Test
	@Transactional
	public void test_get_product_by_saasDatabase_id() {
		product3.addServer(server);
		product3.addServer(server1);
		Product productRes = productRepository.save(product3);
		assertEquals(productRepository.findById(productRes.getProductId()).get().getServers().size(),2);
		assertNotNull(productRepository.findAll());
		assertEquals(productRes.getServers().size(),2);
	}

	@Test
	@Transactional
	public void test_findProductsBySaasDatabasesId() {
		product3.addServer(server);
		Product product = productRepository.save(product3);
		List<Product> productRes = productRepository.findProductsByServersServerId(product.getServers().iterator().next().getServerId());
		assertEquals(productRes.get(0).getServers().iterator().next().getName(), server.getName());
		assertEquals(productRes.get(0).getServers().size(),1);
		assertEquals(productRes.size(),1);
	}

	@Test
	@Transactional
	public void test_findProductsBySaasDatabasesId_get_empty_list() {
		product3.addServer(server);
		Product product = productRepository.save(product3);
		List<Product> productRes = productRepository.findProductsByServersServerId(product.getServers().iterator().next().getServerId()+1);
		assertEquals(productRes.size(),0);
	}

	@Test
	@Transactional
	public void test_findProductBySaasDatabasesIdAndId(){
		product3.addServer(server);
		Product product = productRepository.save(product3);
		Product productRes = productRepository.findProductByServersServerIdAndProductId(product.getServers().iterator().next().getServerId(),product.getProductId());
		assertEquals(productRes.getServers().size(),1);
		assertEquals(productRes.getProductName(),product.getProductName());
		assertEquals(productRes.getServers().iterator().next().getName(), server.getName());
	}

	@Test
	@Transactional
	public void test_findProductBySaasDatabasesIdAndId_get_null(){
		product3.addServer(server);
		Product product = productRepository.save(product3);
		Product productRes = productRepository.findProductByServersServerIdAndProductId(product.getServers().iterator().next().getServerId()+1,product.getProductId());
		assertNull(productRes);
	}
}