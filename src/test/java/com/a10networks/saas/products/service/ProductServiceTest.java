package com.a10networks.saas.products.service;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.repository.ProductRepository;
import com.a10networks.saas.products.util.ProductConstant;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class ProductServiceTest {

    @Autowired
    ProductService productService;

    @Autowired
    ProductRepository productRepository;

    Product product;
    List<Product> productList;

    @BeforeEach
    public void setup(){
        productList = new ArrayList<>();
        Product product1 = new Product(1,"product1");
        Product product2 = new Product(2,"product2");
        productList.add(product1);
        productList.add(product2);
        product = new Product(1,"productNew");
    }

    @AfterEach
    public void tearDown(){
        product = null;
        productRepository.deleteAll();
    }

    // Unit test cases for PATCH and UPDATE

    // Negative Test case for PUT (Update) Method
    @Test
    public void test_update_negative(){
        Product p = productRepository.save(product);
        Product actual = productRepository.findById(p.getProductId()).get();
        actual.setProductName("Aditya");
        Exception e = assertThrows(ProductServiceException.class,()->productService.modify(actual,123)) ;
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC,e.getMessage());
    }

    // Positive test case for PATCH
    @Test
    public void test_updatePartially() {
        Product p = productRepository.save(product);
        Map<String,Object> fields = new HashMap<>();
        fields.put("productName","car");
        Product b = productService.updatePartially(p.getProductId(),fields);
        assertNotEquals(p.getProductName(),b.getProductName());
        assertEquals("car",b.getProductName());
    }

    // First Negative test case for PATCH where invalid ID is given
    @Test
    public void test_updatePartially_negative1() {
        Product p = productRepository.save(product);
        Map<String,Object> fields = new HashMap<>();
        fields.put("productName","car");
        Exception e = assertThrows(ProductServiceException.class,()-> productService.updatePartially(1000,fields)) ;
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC,e.getMessage());
    }

    // Second Negative test case for PATCH where invalid columns are given
    @Test
    public void test_updatePartially_negative2() {
        Product g = productRepository.save(product);
        Map<String,Object> fields = new HashMap<>();
        fields.put("size","4GB");
        fields.put("Ram",16.8);
        Exception e = assertThrows(ProductServiceException.class,()-> productService.updatePartially(g.getProductId(),fields));
        assertEquals(ProductConstant.INVALID_COLUMN_DESC,e.getMessage());
    }
}
