package com.a10networks.saas.products.service;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.repository.ProductRepository;
import com.a10networks.saas.products.service.impl.ProductServiceImpl;
import com.a10networks.saas.products.util.ProductConstant;
import org.hibernate.HibernateException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

import javax.persistence.PersistenceException;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

/**
 * @author NGupta3
 * @email NGupta3@a10networks.com
 */

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ProductsMockServiceTest {

	@InjectMocks
	ProductServiceImpl productService;
	@Mock
	ProductRepository productRepository;
	private List<Product> productList;

	private Product product;

	@BeforeEach
	public void setup(){
		productList = new ArrayList<Product>();
		Product product1 = new Product("product1");
		Product product2 = new Product("product2");
		productList.add(product1);
		productList.add(product2);
		product = new Product("productNew");
	}

	@AfterEach
	public void tearDown(){
		productRepository.deleteAll();
	}
	@Test
	public void test_CreatedList_findAll() throws ProductServiceException {
		when(productRepository.findAll()).thenReturn(productList);
		List<Product> actual = productService.findAll(null);
		assertNotNull(actual);
		assertEquals(productList.size(), actual.size());
		assertEquals(productList.get(0).getProductName(), actual.get(0).getProductName());
	}
	@Test
	public void test_all_persistenceException() throws ProductServiceException {
		when(productRepository.findAll()).thenThrow(new PersistenceException("DB Exception occured due to XYZ reason."));
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.findAll(null));
		assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
	}

	@Test
	public void test_single() throws ProductServiceException {
		when(productRepository.findById(product.getProductId())).thenReturn(Optional.ofNullable(product));
		final Product actual = productService.single(product.getProductId());
		assertNotNull(actual);
		assertEquals(product.getProductName(), actual.getProductName());
	}

	@Test
	public void test_single_noSuchElementException() throws ProductServiceException {
		when(productRepository.findById(product.getProductId()))
				.thenThrow(new NoSuchElementException("DB Exception occured"));
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.single(product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
	}

	@Test
	public void test_single_emptyResultDataAccessException() throws ProductServiceException {
		when(productRepository.findById(product.getProductId()))
				.thenThrow(new EmptyResultDataAccessException("DB Exception occured due to XYZ reason.", 1));
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.single(product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
	}

	@Test
	public void test_single_Exception() throws ProductServiceException {
		when(productRepository.findById(product.getProductId())).thenThrow(new HibernateException("DB Exception occured due to XYZ reason."));
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.single(product.getProductId()));
		assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
	}

	@Test
	public void test_create() throws ProductServiceException {
		when(productRepository.save(product)).thenReturn(product);
		final Product actual = productService.create(product);
		assertNotNull(actual);
		assertEquals(product.getProductName(), actual.getProductName());
		assertEquals(product.getProductId(), actual.getProductId());
	}
	@Test
	public void test_create_persistenceException() throws ProductServiceException {
		when(productRepository.save(product))
				.thenThrow(new PersistenceException("DB Exception occured"));
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.create(product));
		assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
	}

	@Test
	public void test_update() throws ProductServiceException {
		Product product1 = new Product("product1");
		when(productRepository.findById(product.getProductId())).thenReturn(Optional.ofNullable(product));
		when(productRepository.save(product1)).thenReturn(product1);
		final Product actual = productService.modify(product1, product.getProductId());
		assertNotNull(actual);
		assertEquals(product1.getProductName(), actual.getProductName());
		assertEquals(product1.getProductId(), actual.getProductId());
	}

	@Test
	public void test_modify_noSuchElementException() throws ProductServiceException {
		when(productRepository.findById(product.getProductId())).thenReturn(Optional.ofNullable(product));
		when(productRepository.save(product)).thenThrow(new NoSuchElementException("DB Exception occured due to XYZ reason."));
		final Exception exception = assertThrows(ProductServiceException.class,
				() -> productService.modify(product, product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
	}

	@Test
	public void test_modify_emptyResultDataAccessException() throws ProductServiceException {
		when(productRepository.findById(product.getProductId())).thenReturn(Optional.ofNullable(product));
		when(productRepository.save(product))
				.thenThrow(new EmptyResultDataAccessException("DB Exception occured due to XYZ reason.", 1));
		final Exception exception = assertThrows(ProductServiceException.class,
				() -> productService.modify(product, product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
	}

	@Test
	public void test_modify_Exception() throws ProductServiceException {
		when(productRepository.findById(product.getProductId())).thenReturn(Optional.ofNullable(product));
		when(productRepository.save(product))
				.thenThrow(new DataRetrievalFailureException("DB Exception occured due to XYZ reason."));
		final Exception exception = assertThrows(ProductServiceException.class,
				() -> productService.modify(product, product.getProductId()));
		assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());

	}

	@Test
	public void test_remove() throws ProductServiceException {
		doNothing().when(productRepository).deleteById(product.getProductId());
		productService.remove(product.getProductId());
		verify(productRepository).deleteById(product.getProductId());
	}

	@Test
	public void test_remove_noSuchElementException() throws ProductServiceException {
		Mockito.doThrow(new NoSuchElementException("DB Exception occured due to XYZ reason.")).when(productRepository)
				.deleteById(product.getProductId());
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.remove(product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
	}

	@Test
	public void test_remove_emptyResultDataAccessException() throws ProductServiceException {
		Mockito.doThrow(new EmptyResultDataAccessException("DB Exception occured due to XYZ reason.", 1))
				.when(productRepository).deleteById(product.getProductId());
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.remove(product.getProductId()));
		assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());

	}

	@Test
	public void test_remove_Exception() throws ProductServiceException {
		Mockito.doThrow(new IncorrectResultSizeDataAccessException(1)).when(productRepository).deleteById(product.getProductId());
		final Exception exception = assertThrows(ProductServiceException.class, () -> productService.remove(product.getProductId()));
		assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());

	}
}
