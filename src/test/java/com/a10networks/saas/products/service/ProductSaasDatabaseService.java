package com.a10networks.saas.products.service;


import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.entity.Server;
import com.a10networks.saas.products.entity.Tenant;
import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.repository.ProductRepository;
import com.a10networks.saas.products.util.ProductConstant;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;

/**
 * @author NGupta3
 * @email NGupta3@a10networks.com
 */

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class ProductServerServiceTest {
    @InjectMocks
    ProductServerService productServerService;
    @Mock
    ProductRepository  productRepository;
    @Mock
    ServerServiceClient saaSDatabaseServiceClient;
    private List<Product> prodList, prodSaasDatabaseList;

    private Product product, prodSaasDatabase, prodSaasDatabaseAdd;
    private Server server, server1, serverError;

    @BeforeEach
    public void setup(){
        prodList = new ArrayList<Product>();
        Set<Tenant> tenants = new HashSet<>();
        Set<Product> productSaasDatabase = new HashSet<>();
        Set<Server> serverProduct = new HashSet<>();
        Set<Server> serverProduct1 = new HashSet<>();
        Product product1 = new Product(1,"product1",tenants, serverProduct);
        Product product2 = new Product(2,"product2",tenants, serverProduct);
        prodList.add(product1);
        prodList.add(product2);
        server = new Server(1,"saasDatabase1","type","url","user","password","",new HashMap<>(),1,new Date(),1,new Date(),productSaasDatabase);

        prodSaasDatabaseList =  new ArrayList<Product>();
        prodSaasDatabase = new Product(1,"product1",tenants, serverProduct);
        prodSaasDatabaseAdd = new Product(1,"product2",tenants, serverProduct1);
        server = new Server(1,"saasDatabase1","type","url","user","password","",new HashMap<>(),1,new Date(),1,new Date(),productSaasDatabase);
        server1 = new Server(2, "saasDatabase1","type","url","user","password","",new HashMap<>(),1,new Date(),1,new Date(),productSaasDatabase);
        serverError = new Server();
        prodSaasDatabase.addServer(server1);
        prodSaasDatabaseList.add(prodSaasDatabase);
    }

    @AfterEach
    public void tearDown(){
        productRepository.deleteAll();
    }

    @Test
    public void test_getProductsBySaasDatabaseId() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        given(productRepository.findProductsByServersServerId(saasDatabaseId)).willReturn(prodSaasDatabaseList);
        final List<Product> actual = productServerService.getProductsByServerId(saasDatabaseId);
        assertNotNull(actual);
        assertEquals(actual.size(),1);
        assertEquals(actual.get(0).getProductName(), prodSaasDatabase.getProductName());
        assertEquals(actual.get(0).getServers().iterator().next().getName(), server1.getName());
    }

    @Test
    public void test_getProductsBySaasDatabaseId_throw_no_data_found() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(null);
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.getProductsByServerId(saasDatabaseId));
        assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
    }

    @Test
    public void test_getProductsBySaasDatabaseId_throw_NoSuchElementException() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        when(productRepository.findProductsByServersServerId(saasDatabaseId))
                .thenThrow(new NoSuchElementException("DB Exception occured"));
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.getProductsByServerId(saasDatabaseId));
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
    }

    @Test
    public void test_getProductBySaasDatabaseAndProductId() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        given(productRepository.findProductByServersServerIdAndProductId(saasDatabaseId, prodSaasDatabase.getProductId())).willReturn(prodSaasDatabase);
        final Product actual = productServerService.getProductByServerAndProductId(saasDatabaseId, prodSaasDatabase.getProductId());
        assertNotNull(actual);
        assertEquals(actual.getProductName(), prodSaasDatabase.getProductName());
        assertEquals(actual.getServers().iterator().next().getName(), server1.getName());
    }

    @Test
    public void test_getProductBySaasDatabaseAndProductId_server_error() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(null);
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.getProductByServerAndProductId(saasDatabaseId, prodSaasDatabase.getProductId()));
        assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
    }

    @Test
    public void test_getProductBySaasDatabaseAndProductId_NoSuchElementException() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        when(productRepository.findProductByServersServerIdAndProductId(saasDatabaseId, prodSaasDatabase.getProductId()))
                .thenThrow(new NoSuchElementException("DB Exception occured"));
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.getProductByServerAndProductId(saasDatabaseId, prodSaasDatabase.getProductId()));
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
    }

    @Test
    public void test_addProductSaasDatabaseMapping() throws ProductServiceException {
        int saasDatabaseId = 1;
        assertEquals(prodSaasDatabaseAdd.getServers().size(), 0);
        when(productRepository.findById(prodSaasDatabaseAdd.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabaseAdd));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        when(productRepository.save(prodSaasDatabaseAdd)).thenReturn(prodSaasDatabaseAdd);
        productServerService.addProductServerMapping(saasDatabaseId, prodSaasDatabaseAdd.getProductId());
        assertNotNull(prodSaasDatabaseAdd.getServers().iterator().next().getName(), server.getName());
        assertEquals(prodSaasDatabaseAdd.getServers().size(), 1);
    }
    @Test
    public void test_addProductSaasDatabaseMapping_server_error() throws ProductServiceException {
        int saasDatabaseId = 1;
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(null);
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.addProductServerMapping(saasDatabaseId, prodSaasDatabaseAdd.getProductId()));
        assertEquals(ProductConstant.FAILED_TO_PROCESS_DESC, exception.getMessage());
    }

    @Test
    public void test_addProductSaasDatabaseMapping_NoSuchElementException() throws ProductServiceException {
        int saasDatabaseId = 1;
        assertEquals(prodSaasDatabaseAdd.getServers().size(), 0);
        when(productRepository.findById(prodSaasDatabaseAdd.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabaseAdd));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server);
        when(productRepository.save(prodSaasDatabaseAdd))
                .thenThrow(new NoSuchElementException("DB Exception occured"));
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.addProductServerMapping(saasDatabaseId, prodSaasDatabaseAdd.getProductId()));
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
    }

    @Test
    public void test_delProductsSaasDatabaseMapping() throws ProductServiceException {
        int saasDatabaseId = 1;
        assertEquals(prodSaasDatabase.getServers().size(), 1);
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server1);
        productServerService.delProductsServerMapping(saasDatabaseId, prodSaasDatabase.getProductId());
        assertEquals(prodSaasDatabase.getServers().size(), 0);
    }

    @Test
    public void test_delProductsSaasDatabaseMapping_server_error() throws ProductServiceException {
        int saasDatabaseId = 1;
        assertEquals(prodSaasDatabase.getServers().size(), 1);
        when(productRepository.findById(prodSaasDatabase.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabase));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(serverError);
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.delProductsServerMapping(saasDatabaseId, prodSaasDatabase.getProductId()));
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
    }

    @Test
    public void test_delProductsSaasDatabaseMapping_unable_to_process() throws ProductServiceException {
        int saasDatabaseId = 1;
        assertEquals(prodSaasDatabase.getServers().size(), 1);
        when(productRepository.findById(prodSaasDatabaseAdd.getProductId())).thenReturn(Optional.ofNullable(prodSaasDatabaseAdd));
        when(saaSDatabaseServiceClient.getServerByServerId(saasDatabaseId)).thenReturn(server1);
        final Exception exception = assertThrows(ProductServiceException.class, () -> productServerService.delProductsServerMapping(saasDatabaseId, prodSaasDatabase.getProductId()));
        assertEquals(ProductConstant.NO_DATA_FOUND_DESC, exception.getMessage());
    }
}
