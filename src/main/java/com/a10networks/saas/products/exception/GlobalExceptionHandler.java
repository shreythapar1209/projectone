package com.a10networks.saas.products.exception;

import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author DLohia
 * @email DLohia@a10networks.com
 */

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	private static final String ERROR_CODE = "errorCode";
	private static final String MESSAGE = "message";

	@ExceptionHandler(ProductServiceException.class)
	public ResponseEntity<Object> handleProductServiceException(final ProductServiceException ex, final WebRequest request) {
		List<String> details =new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(),details,ex.getErrorCode());
		return new ResponseEntity<>(error,HttpStatus.INTERNAL_SERVER_ERROR);

		//eturn handleCustomException(ex, request, ex.getErrorCode());
	}

	private ResponseEntity<Object> handleCustomException(ProductServiceException ex, WebRequest request, HttpStatus httpStatus) {
		final Map<String, Object> body = new LinkedHashMap<String, Object>();
		body.put(MESSAGE, ex.getMessage());
		return new ResponseEntity<>(body,httpStatus);
	}
	@ExceptionHandler(RecordNotFoundException.class)
	public ResponseEntity<Object> handleRecordNotFoundException(final RecordNotFoundException ex, final WebRequest request){
		List<String> details =new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(),details,ex.getErrorCode());
		return new ResponseEntity<>(error,HttpStatus.NOT_FOUND);
	}
	@ExceptionHandler(InvalidArgumentsException.class)
	public ResponseEntity<Object> handleInvalidArgumentsException(final InvalidArgumentsException ex, final WebRequest request){
		List<String> details =new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(),details,ex.getErrorCode());
		return new ResponseEntity<>(error,HttpStatus.BAD_REQUEST);
	}
	@ExceptionHandler(ProductNameExistsException.class)
	public ResponseEntity<Object> handleUserEmailExistsException(final ProductNameExistsException ex, final WebRequest request){
		List<String> details =new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(ex.getMessage(),details,ex.getErrorCode());
		return new ResponseEntity<>(error,HttpStatus.UNPROCESSABLE_ENTITY);
	}


	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for(ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}

		ErrorResponse error = new ErrorResponse("Validation Failed", details, ProductConstant.BAD_REQUEST);

		return new ResponseEntity(error, HttpStatus.BAD_REQUEST);
	}
	@Override
	protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest  request)
	{
		List<String> details = new ArrayList<>();
		details.add("Json Parse Error");


		ErrorResponse error = new ErrorResponse("Invalid field provided", details, ProductConstant.BAD_REQUEST);


		return new ResponseEntity<>(error,HttpStatus.UNPROCESSABLE_ENTITY);
	}



}
