package com.a10networks.saas.products.exception;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Getter
@Setter
@XmlRootElement(name = "error")
public class ErrorResponse
{
    public ErrorResponse(String message, List<String> details, String errorCode) {
        super();
        this.message = message;
        this.details = details;
        this.errorCode = errorCode;
    }

    private String errorCode;
    private String message;

    private List<String> details;

}
