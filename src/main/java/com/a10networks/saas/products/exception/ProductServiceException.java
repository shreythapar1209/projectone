package com.a10networks.saas.products.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

/**
 * @author DLohia
 * @email DLohia@a10networks.com
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProductServiceException extends RuntimeException {
	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String message;
}
