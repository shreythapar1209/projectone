package com.a10networks.saas.products.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class UserInfo implements Serializable {
    private static final long serialVersionUID = 1L;
    private int emp_id;
    private String name;
}

