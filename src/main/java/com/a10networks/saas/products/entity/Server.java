package com.a10networks.saas.products.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author SMundhe
 * @email SMundhe@a10networks.com
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "server",uniqueConstraints = { @UniqueConstraint(name = "server_UN_name", columnNames = { "name" }) })
public class Server implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="server_id")
	private int serverId;

	@NotNull(message = "Database name cannot be null")
	@Column(name="name",nullable = false,columnDefinition = "varchar(32)")
	private String name;
	@NotNull(message = "Type cannot be null")
	@Column(name = "type", nullable = false,columnDefinition = "varchar(32) comment 'MySQL,Redis,Kafka,OpenSearch,Email'")
	private String type;
	@NotNull(message = "Url cannot be null")
	@Column(name = "url", nullable = false,columnDefinition = "varchar(512)")
	private String url;
	@NotNull(message = "user cannot be null")
	@Column(name = "user",columnDefinition = "varchar(64)")
	private String user;
	@NotNull(message = "password cannot be null")
	@Column(name = "password",columnDefinition = "varchar(64)")
	private String password;
	@NotNull(message = "driver cannot be null")
	@Column(name="driver",columnDefinition = "varchar(64)")
	private String driver;

	@Type(type = "json")
	@Column(name = "info", columnDefinition = "json default null")
	private Map<String,String> info;
	@Column(name="created_by_id",columnDefinition = "int default null")
	private int createdById;
	@CreationTimestamp
	@Column(name="create_date",columnDefinition = "date default null")
	private Date createDate;
	@Column(name="last_modified_by_id",columnDefinition = "int default null")
	private int lastModifiedById;
	@UpdateTimestamp
	@Column(name="last_modified_date",columnDefinition = "date default null")
	private Date lastModifiedDate;

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			},
			mappedBy = "servers")
	@JsonIgnore
	private Set<Product> products = new HashSet<>();

	public Server(String name, String type, String url, String user, String password, String driver) {
		this.name = name;
		this.type = type;
		this.url = url;
		this.user = user;
		this.password = password;
		this.driver= driver;
	}
}