package com.a10networks.saas.products.entity;

import com.a10networks.saas.products.exception.InvalidArgumentsException;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author SSingh
 * @email ssingh2@a10networks.com
 */
@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "product",uniqueConstraints = { @UniqueConstraint(name = "product_UN", columnNames = { "product_name" }) })
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class Product implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "product_id", unique = true)
	private int productId;
	@Column(name = "product_name", nullable = false,columnDefinition = "varchar(32)")
	@NotNull(message = "Product name cannot be null")
	@JsonProperty(required = true)
	private String productName;

	@Column(name = "base_url",columnDefinition = "varchar(128)",nullable = true)
	private String baseUrl;

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			},
			mappedBy = "products")
	@JsonIgnore
	private Set<Tenant> tenants = new HashSet<>();
	@CreationTimestamp
	@Column(name = "create_date",nullable=false, updatable=false)
	private Date createDate;
	@UpdateTimestamp
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;

	@Column(name = "created_by_id")
	private Integer createdById;
	@Column(name = "last_modified_by_id")
	private Integer lastModifiedById;

	@PrePersist
	private void beforeSaving() {
		createdById = (int) Thread.currentThread().getId();
	}

	@PreUpdate
	private void beforeUpdating() {
		lastModifiedById = (int) Thread.currentThread().getId();
	}

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			})
	@JoinTable(name = "product_server_rel",
			joinColumns = { @JoinColumn(name = "product_id") },
			inverseJoinColumns = { @JoinColumn(name = "server_id") })
	private Set<Server> servers = new HashSet<>();

	public Product(String productName) {
		super();
		this.productName = productName;
	}

	public Product(int id,String productName) {
		super();
		this.productId = id;
		this.productName = productName;
	}

	public Product(int id, String productName, Set<Tenant> tenants, Set<Server> servers) {
		super();
		this.productId = id;
		this.productName = productName;
		this.tenants = tenants;
		this.servers = servers;
	}

	public void addServer(Server server) {
		this.servers.add(server);
		server.getProducts().add(this);
	}

	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			},
			mappedBy = "products")
	@JsonIgnore
	private Set<User> users = new HashSet<>();

}
