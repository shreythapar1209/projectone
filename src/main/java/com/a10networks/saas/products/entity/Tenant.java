package com.a10networks.saas.products.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.*;

/**
 * @author NGupta3
 * @email NGupta3@a10networks.com
 */

@Getter
@Setter
@AllArgsConstructor
@TypeDef(name = "json", typeClass = JsonStringType.class)
@NoArgsConstructor
@Table(name = "tenant",uniqueConstraints = { @UniqueConstraint(name = "tenant_UN", columnNames = { "tenant_name" }) })
@Entity
public class Tenant implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "tenant_id", unique = true)
	private Integer tenantId;

	@Column(name = "company_name",nullable = false,columnDefinition = "varchar(128)")
	@NotNull(message = "company_name cannot be null")
	@Size(message = "company name should be between 0 to 128 characters")
	private String companyName;

	@Column(name = "subsidiary_name",columnDefinition = "varchar(64)")
	@Size(message = "subsidiary name should be between 0 to 64 characters")
	private String subsidiaryName;

	@Column(name = "website_url",columnDefinition = "varchar(2048)",nullable = true)
	private String websiteUrl;

	@Column(name = "street",columnDefinition = "varchar(128)",nullable = true)
	private String street;

	@Column(name = "city",columnDefinition = "varchar(64)",nullable = true)
	private String city;

	@Column(name = "state",columnDefinition = "varchar(32)",nullable = true)
	private String state;

	@Column(name = "zip",columnDefinition = "varchar(16)",nullable = true)
	private String zip;

	@Column(name = "country",columnDefinition = "varchar(128)",nullable = true)
	private String country;

	@Column(name = "phone",columnDefinition = "varchar(32)",nullable = true)
	private String phone;

	@Column(name = "primary_contact",columnDefinition = "varchar(128)",nullable = true)
	private String primaryContact;

	@Column(name = "subscription_tier",columnDefinition = "varchar(64) comment 'Fremium,Enterprise,etc'",nullable = true)
	private String subscriptionTier;

	@Column(name = "tenant_name",columnDefinition = "varchar(32)",nullable = false)
	@NotNull(message="Tenant name cannot be null")
	@Size(message = "Tenant Name should be between 2 to 20 characters.")
	private String tenantName;

	@Column(name = "unique_tenant_id", columnDefinition = "BINARY(16)", unique = true)
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID uniqueTenantId;

	@Type(type = "json")
	@Column(name = "tenant_info", columnDefinition = "json")
	private Map<String,String> tenantInfo;
	@Column(name = "is_deleted", nullable = false,columnDefinition="tinyint(1) default 0")
	private boolean isDeleted;

	@Column(name = "is_enabled",nullable = false,columnDefinition="tinyint(1) default 1")
	private boolean isEnabled;

	@Column(name = "status",columnDefinition = "varchar(64) default null comment 'onboarding-in-progress,on-boarded,deleted,purged,etc'")
	private String status;

	@Column(name = "external_user_id",columnDefinition = "varchar(64) default null comment 'Temporary usr-id that is used for self-onboarding,end-user email'")
	private String externalUserId;

	@CreationTimestamp
	@Column(name = "create_date",nullable=false, updatable=false)
	private Date createDate;
	@UpdateTimestamp
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;
	@Column(name = "created_by_id")
	private int createdById;
	@Column(name = "last_modified_by_id")
	private int lastModifiedById;

	@PrePersist
	private void beforeSaving() {
		createdById = (int) Thread.currentThread().getId();
		uniqueTenantId = UUID.randomUUID();
	}

	@PreUpdate
	private void beforeUpdating() {
		lastModifiedById = (int) Thread.currentThread().getId();
	}

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = {
					CascadeType.PERSIST,
					CascadeType.MERGE
			})
	@JoinTable(name = "tenant_product_rel",
			joinColumns = { @JoinColumn(name = "tenant_id") },
			inverseJoinColumns = { @JoinColumn(name = "product_id") })
	private Set<Product> products = new HashSet<>();

	public Tenant(Integer tenantId, String tenantName) {
		super();
		this.tenantName = tenantName;
		this.tenantId = tenantId;
	}
	public void addProduct(Product product) {
		this.products.add(product);
		product.getTenants().add(this);
	}
	public Tenant(String tenantName, boolean isDeleted,String companyName ){
		super();
		this.tenantName = tenantName;
		this.isDeleted = isDeleted;
		this.companyName=companyName;
	}

}
