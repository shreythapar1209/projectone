package com.a10networks.saas.products.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name="`group`")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id", nullable = false, unique = true)
    private int groupId;

    @Column(name = "tenant_id", nullable = false)
    @NotNull(message = "Tenant id cannot be null")
    private int tenantId;

    @Column(name = "group_name", nullable = false,columnDefinition = "varchar(128)")
    @NotNull(message = "Group name cannot be null")
    private String groupName;

    @CreationTimestamp
    @Column(name = "create_date",nullable=false, updatable=false)
    private Date createDate;

    @UpdateTimestamp
    @Column(name = "last_modified_date")
    private Date lastModifiedDate;

    @Column(name = "created_by_id")
    private int createdById;
    @Column(name = "last_modified_by_id")
    private int lastModifiedById;

    @PrePersist
    private void beforeSaving() {
        createdById = (int) Thread.currentThread().getId();
    }

    @PreUpdate
    private void beforeUpdating() {
        lastModifiedById = (int) Thread.currentThread().getId();
    }

    @ManyToMany(fetch = FetchType.LAZY,
            mappedBy = "groups",
            cascade = {
                    CascadeType.MERGE,
                    CascadeType.PERSIST
            })
    @JsonIgnore
    private Set<User> users = new HashSet<>();

    public Group(int tenantId, String groupName, Timestamp createDate, Timestamp lastModifiedDate) {
        this.tenantId = tenantId;
        this.groupName = groupName;
        this.createDate = createDate;
        this.lastModifiedDate = lastModifiedDate;
    }


    public void addUser(User user) {
        this.users.add(user);
        user.getGroups().add(this);
    }

}
