package com.a10networks.saas.products.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * @author SMundhe
 * @email SMundhe@a10networks.com
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user")
@TypeDef(name = "json", typeClass = JsonStringType.class)
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="user_id",nullable = false, unique = true)
	private int userId;

	@NotNull(message = "Tenant id cannot be null")
	@Column(name = "tenant_id", nullable = false)
	private int tenantId;

	@NotNull(message = "Tenant sec id cannot be null")
	@Column(name = "tenant_sec_id")
	private int tenantSecId;

	@Column(name = "first_name",columnDefinition = "varchar(64)",nullable = true)
	private String firstName;

	@Column(name = "last_name",columnDefinition = "varchar(64)",nullable = true)
	private String lastName;

	@Email(message = "Email should be valid")
	@Column(name="email",nullable = false, unique = true)
	private String email;

	@NotNull(message = "External user id cannot be null")
	@Column(name = "external_user_id",nullable = false,columnDefinition = "varchar(128)")
	private String externalUserId;

	@Column(name = "last_successful_login",columnDefinition = "datetime")
	private Date lastSuccessfulLogin;

	@Column(name = "created_by_id",nullable = true)
	private int createdById;

	@Column(name = "last_modified_by_id",nullable = true)
	private int lastModifiedById;

	@CreationTimestamp
	@Column(name = "create_date")
	private Date createDate;

	@UpdateTimestamp
	@Column(name = "last_modified_date")
	private Date lastModifiedDate;

	@Column(name = "last_failed_login")
	private Date lastFailedLogin;

	@Column(name = "total_successful_logins")
	private Integer totalSuccessfulLogins;

	@Column(name = "latest_failed_login_attempts")
	private Integer latestFailedLoginsAttempts;

	@NotNull(message = "Eula Agreed cannot be null")
	@Column(name = "eula_agreed",columnDefinition = "boolean default '0'")
	private boolean eulaAgreed;

	@NotNull(message = "Eula show cannot be null")
	@Column(name = "eula_show",columnDefinition = "boolean default '1'")
	private boolean eulaShow;

	@NotNull(message = "blocked cannot be null")
	@Column(columnDefinition = "boolean default false")
	private boolean blocked;

	@PrePersist
	private void beforeSaving() {
		createdById = (int)Thread.currentThread().getId();
		lastSuccessfulLogin = new Date();
	}

	@PreUpdate
	private void beforeUpdating() {
		lastModifiedById = (int)Thread.currentThread().getId();
		lastSuccessfulLogin = new Date();
	}


	@Type(type = "json")
	@Column(name = "user_info",columnDefinition = "json")
	private UserInfo userInfo;

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY ,
			cascade = {
					CascadeType.MERGE,
					CascadeType.PERSIST
			})
	@JoinTable(name = "user_group_rel",
			joinColumns = {@JoinColumn(name = "userId")},
			inverseJoinColumns = {@JoinColumn(name = "group_id")})
	private Set<Group> groups = new HashSet<>();

	@JsonIgnore
	@ManyToMany(fetch = FetchType.LAZY ,
			cascade = {
					CascadeType.MERGE,
					CascadeType.PERSIST
			})
	@JoinTable(name = "user_product_rel",
			joinColumns = {@JoinColumn(name = "userId")},
			inverseJoinColumns = {@JoinColumn(name = "productId")})
	private Set<Product> products = new HashSet<>();

	public User(int tenantId, int tenantSecId, String firstName, String lastName, String email, Date lastSuccessfulLogin, int createdById, Date createDate, int lastModifiedById, Date lastModifiedDate) {
		this.tenantId = tenantId;
		this.tenantSecId = tenantSecId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.lastSuccessfulLogin = lastSuccessfulLogin;
		this.createdById = createdById;
		this.createDate = createDate;
		this.lastModifiedById = lastModifiedById;
		this.lastModifiedDate = lastModifiedDate;
	}

	public User(int userId, int tenantId, int tenantSecId, String firstName, String lastName, String email, Date lastSuccessfulLogin, int createdById, Date createDate, int lastModifiedById, Date lastModifiedDate, String externalUserId) {
		this.userId = userId;
		this.tenantId = tenantId;
		this.tenantSecId = tenantSecId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.lastSuccessfulLogin = lastSuccessfulLogin;
		this.createdById = createdById;
		this.createDate = createDate;
		this.lastModifiedById = lastModifiedById;
		this.lastModifiedDate = lastModifiedDate;
		this.userInfo = userInfo;
		this.externalUserId=externalUserId;
	}

	public void addGroup(Group group) {
		this.groups.add(group);
		group.getUsers().add(this);
	}

	public void addProduct(Product product) {
		this.products.add(product);
		product.getUsers().add(this);
	}
}
