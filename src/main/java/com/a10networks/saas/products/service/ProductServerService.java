package com.a10networks.saas.products.service;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.entity.Server;
import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.exception.RecordNotFoundException;
import com.a10networks.saas.products.repository.ProductRepository;
import com.a10networks.saas.products.util.ProductConstant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Optional;

/**
 * @author SSingh
 * @email ssingh2@a10networks.com
 */

@Service
public class ProductServerService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    ServerServiceClient serverServiceClient;
    private static final Logger LOG = LoggerFactory.getLogger(ProductServerService.class);

    public List<Product> getProductsByServerId(int serverId) {
        try {
            Server _server = serverServiceClient.getServerByServerId(serverId);
            if (_server==null) {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
            }
            else{
            List<Product> products = productRepository.findProductsByServersServerId(serverId);
            if(products.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
          else{  return products;}}
        } catch (NoSuchElementException | EmptyResultDataAccessException | HttpClientErrorException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, serverId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }

    public Product getProductByServerAndProductId(int serverId, int productId) {
        try {
            Optional<Product> _product = productRepository.findById(productId);
            LOG.info("Tenant " + _product);
            if (_product.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
               else {
                    Server _Server = serverServiceClient.getServerByServerId(serverId);
                    LOG.info("_Server " + _Server.getName());
                    if (_Server==null){
                        throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

                    }
                    else{
                        Product productRes = productRepository.findProductByServersServerIdAndProductId(serverId, productId);
                        if(productRes==null)
                        {
                            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

                        }
                      else{  return productRes;}
                    } }
            } catch (NoSuchElementException | EmptyResultDataAccessException | HttpClientErrorException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, productId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }

    public void addProductServerMapping(int serverId, int productId) {
        try {
            LOG.info("serverId " + serverId + "productId " + productId);
            Optional<Product> _product = productRepository.findById(productId);
            LOG.info("Tenant " + _product);
            if (_product.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
          else {
                Product product = _product.get();
                Server _Server = serverServiceClient.getServerByServerId(serverId);
                LOG.info("_Server " + _Server.getName());
                if (_Server == null) {
                    throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

                } else {
                    product.addServer(_Server);
                    productRepository.save(product);
                }
            }} catch (NoSuchElementException | EmptyResultDataAccessException | HttpClientErrorException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, productId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } catch (IllegalStateException | InvalidDataAccessApiUsageException e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.BAD_REQUEST,
                    "relation already exist");
        }catch(DataIntegrityViolationException ex)
        { LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, ex);
            throw new ProductServiceException(ProductConstant.BAD_REQUEST,
                    "relation already exist");


        }
    }

    @Transactional
    public void delProductsServerMapping(int serverId, int productId) {
        try {
            System.out.println(serverId);
            Product product = productRepository.findById(productId).get();
            LOG.info("Tenant " + product);
            if (!Optional.ofNullable(product.getProductId()).isEmpty()) {
                Server _server = serverServiceClient.getServerByServerId(serverId);
                LOG.info("_server " + _server.getName());
                if (!Optional.ofNullable(_server.getServerId()).isEmpty()) {
                    boolean isDeleted = product.getServers().removeIf(server -> {
                        return Objects.equals(_server.getServerId(), server.getServerId());
                    });
                    if(!isDeleted)
                        throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
                } else
                    throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
            } else
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } catch (NoSuchElementException | EmptyResultDataAccessException | HttpClientErrorException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, productId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }
}