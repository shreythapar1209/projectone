package com.a10networks.saas.products.service.impl;

import java.util.*;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.entity.Tenant;
import com.a10networks.saas.products.exception.InvalidArgumentsException;
import com.a10networks.saas.products.exception.ProductNameExistsException;
import com.a10networks.saas.products.exception.RecordNotFoundException;
import com.a10networks.saas.products.service.ProductService;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.repository.ProductRepository;
import com.a10networks.saas.products.util.ProductConstant;

/**
 * @author SSingh
 * @email ssingh2@a10networks.com
 */
@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;
    private static final Logger LOG = LoggerFactory.getLogger(ProductService.class);

    /**
     * Fetch all products.
     *
     * @return List<{ @ link ProductEntity }> all products
     * @throws {@link ProductServiceException} if the request can not be processed due
     *                to any technical reason.
     *                *
     */
    public List<Product> findAll(String productName)  {
        try {
            List<Product> products ;
            Product product;
            if (productName == null)
                products = productRepository.findAll();
            else
            { product=productRepository.findByProductName(productName);
            if(product==null){
                throw new RecordNotFoundException(ProductConstant.EMPTY_TABLE, ProductConstant.EMPTY_TABLE_DESC);
            }
            else {
                products=Collections.singletonList(product);
            }
            }
            if (products.size() == 0) {
                throw new RecordNotFoundException(ProductConstant.EMPTY_TABLE, ProductConstant.EMPTY_TABLE_DESC);
            }
            else{
            return products;}
        } catch (NoSuchElementException |NullPointerException| EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, productName, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC
            );
        } /*catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }*/
    }

    /**
     * Returns all new products record created.
     *
     * @param product records to be created
     * @return List<{ @ link ProductEntity }> products after creation
     * @throws {@link IllegalArgumentException} if the request contains invalid
     *                value or does not contain mandatory fields.
     * @throws {@link ProductServiceException} if the request can not be processed due
     *                to any technical reason.
     * @since v1.0
     */
    public Product create(final Product product) throws ProductServiceException {
        try {
            Product product1=productRepository.findByProductName(product.getProductName());
            if(product1!=null){
                throw new ProductNameExistsException(ProductConstant.DUPLICATE_ENTRY,ProductConstant.PRODUCT_ALREADY_EXIST_DESC);
            }
            return productRepository.save(product);
        }
        catch(ProductNameExistsException e){
            throw new ProductNameExistsException(ProductConstant.DUPLICATE_ENTRY,ProductConstant.PRODUCT_ALREADY_EXIST_DESC);
        }
        catch (DataIntegrityViolationException e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductNameExistsException(ProductConstant.UNPROCESSABLE_ENTITY, e.getRootCause().toString());
        }
        catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }
    }

    /**
     * Returns individual product record.
     *
     * @param id {@link Integer} type system identification of product
     * @return {@link Product}
     * @throws {@link IllegalArgumentException} if the request contains invalid
     *                value or does not contain mandatory fields.
     * @throws {@link ProductServiceException} if the request can not be processed due
     *                to any technical reason.
     * @since v1.0
     */
    public Product single(final int id) throws ProductServiceException {
        try {
            Product product=productRepository.findById(id).get();
            if(product==null)
            {
                throw new RecordNotFoundException(ProductConstant.INVALID_ID, ProductConstant.EMPTY_TABLE_DESC);

            }
            else{return product;}

        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, id, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }
    }

    /**
     * Returns modified product record.
     *
     * @param product {@link Product} to be modified
     * @param id      {@link Integer} type system identification of product whose record to
     *                be modified
     * @return Product product latest record after creation
     * @throws {@link IllegalArgumentException} if the request contains invalid
     *                value or does not contain mandatory fields.
     * @throws {@link ProductServiceException} if the request can not be processed due
     *                to any technical reason.
     * @since v1.0
     */
    public Product modify(final Product product, final int id)  {

        try {
            Optional<Product> _product = productRepository.findById(id);
            if(_product.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
            }
            Product product2=productRepository.findByProductName(product.getProductName());
            if(product2!=null && product2.getProductId()!= product.getProductId()){
                throw new ProductNameExistsException(ProductConstant.DUPLICATE_ENTRY,ProductConstant.PRODUCT_ALREADY_EXIST_DESC);
            }
            Product product1=_product.get();
            product1.setProductName(product.getProductName());
            return productRepository.save(product1);
        }
        catch (DataIntegrityViolationException e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductNameExistsException(ProductConstant.UNPROCESSABLE_ENTITY, e.getLocalizedMessage());
        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, id, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } /*catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }*/
    }

    /**
     * Remove record from system.
     *
     * @param id {@link Long} type system identification of product whose record to be
     *           deleted.
     * @throws {@link IllegalArgumentException} if the request contains invalid
     *                value or does not contain mandatory fields.
     * @throws {@link ProductServiceException} if the request can not be processed due
     *                to any technical reason.
     * @since v1.0
     */
    public void remove(final int id) throws ProductServiceException {
        try {
            Optional<Product> _product = productRepository.findById(id);
            if(_product.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }

            productRepository.deleteById(id);
        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, id, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }

    public ProductRepository getProductRepository() {
        return productRepository;
    }

    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    public List<Tenant> getTenants(int productId) throws ProductServiceException {
        if (!productRepository.existsById(productId)) {
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
        List<Tenant> tenants = null;
        return tenants;
    }

    public List<Product> getProductsByTenantId(int tenantId) {
        try {

            List<Product> products = productRepository.findProductsByTenantsTenantId(tenantId);
           if(products.isEmpty())
           {
               throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

           }
            return products;
        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, tenantId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }




    public Product updatePartially(int id, Map<String, Object> fields) {

        try {
            Optional<Product> _product = productRepository.findById(id);
            if(_product.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
            else{
                ObjectMapper obj = new ObjectMapper();
                Product product1 = _product.get();
                product1 = obj.updateValue(product1, fields);
                Product product2 = productRepository.findByProductName(product1.getProductName());
                if (product2 != null && product2.getProductId() != product1.getProductId()) {
                    throw new ProductNameExistsException(ProductConstant.DUPLICATE_ENTRY, ProductConstant.PRODUCT_ALREADY_EXIST_DESC);
                }

                return productRepository.save(product1);
            }
        } catch (NoSuchElementException | NullPointerException | EmptyResultDataAccessException | RecordNotFoundException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, id, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } catch(ProductNameExistsException e){
            throw new ProductNameExistsException(ProductConstant.DUPLICATE_ENTRY,ProductConstant.PRODUCT_ALREADY_EXIST_DESC);
        }
        catch(DataIntegrityViolationException e){
            throw new ProductNameExistsException(ProductConstant.UNPROCESSABLE_ENTITY,e.getLocalizedMessage());
        }
        catch(InvalidFormatException ex)
        {
            throw new InvalidArgumentsException(ProductConstant.BAD_REQUEST,ProductConstant.INVALID_COLUMN_DESC);
        }
        catch ( JsonMappingException u) {
            LOG.error(ProductConstant.INVALID_COLUMN, id, u);
            throw new InvalidArgumentsException(ProductConstant.BAD_REQUEST, ProductConstant.INVALID_COLUMN_DESC);
        } catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }
    }

    @Override
    public List<Product> getProductsByUserId(Integer userId) {
        try {

            List<Product> products = productRepository.findProductsByUsersUserId(userId);
            if(products.isEmpty())
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
           else{ return products;}
        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, userId, e);
            throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        } /*catch (Exception e) {
            LOG.error(ProductConstant.FAILED_TO_PROCESS_DESC, e);
            throw new ProductServiceException(ProductConstant.SERVER_ERROR, ProductConstant.FAILED_TO_PROCESS_DESC);
        }*/
    }

    public Product getProductByBaseUrl(String baseUrl){
        try {
            Product product = productRepository.findProductByBaseUrl(baseUrl);
            if(product == null)
            {
                throw new RecordNotFoundException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);

            }
            return product;
        } catch (NoSuchElementException | EmptyResultDataAccessException e) {
            LOG.error(ProductConstant.NO_DATA_FOUND_DESC, e);
            throw new ProductServiceException(ProductConstant.NOT_FOUND, ProductConstant.NO_DATA_FOUND_DESC);
        }
    }
}