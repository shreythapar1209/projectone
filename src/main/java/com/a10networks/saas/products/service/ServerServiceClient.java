package com.a10networks.saas.products.service;

import com.a10networks.saas.products.entity.Server;
import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.exception.RecordNotFoundException;
import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class ServerServiceClient {
    @Autowired
    private RestTemplate serverRestTemplate;

    private static String apiVersion="/api/v1";

    @Value("${data.service.server.url}")
    private String serverServiceUrl;

    public Server getServerByServerId(int serverId) throws ProductServiceException {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("serverId", String.valueOf(serverId));
       try {
           var server = serverRestTemplate.getForObject(serverServiceUrl + apiVersion + "/server/{serverId}", Server.class, uriVariables);
           return server;
       }catch(HttpClientErrorException ex)
       {
           throw new RecordNotFoundException(ProductConstant.NOT_FOUND,ProductConstant.NO_DATA_FOUND_DESC);
       }
    }

}
