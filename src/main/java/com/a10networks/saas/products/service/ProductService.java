package com.a10networks.saas.products.service;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.entity.Tenant;
import com.a10networks.saas.products.repository.ProductRepository;

import java.util.List;
import java.util.Map;

public interface ProductService {
    List<Tenant> getTenants(int productId);
    void setProductRepository(ProductRepository productRepository);
    ProductRepository getProductRepository();
    void remove(final int id);
    Product modify(final Product product, final int id);
    Product single(final int id);
    Product create(final Product product);
    List<Product> findAll(String productName);
    List<Product> getProductsByTenantId(int tenantId);
    Product updatePartially(int id, Map<String, Object> fields);
    Product getProductByBaseUrl(String baseUrl);
    List<Product> getProductsByUserId(Integer userId);
}
