package com.a10networks.saas.products.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.a10networks.saas.products.entity.Product;

import java.util.List;

/**
 * @author SSingh
 * @email ssingh2@a10networks.com
 */

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer>, JpaSpecificationExecutor<Product> {
    List<Product> findProductsByTenantsTenantId(int tenantId);
    List<Product> findProductsByServersServerId(int serverId);
    Product findProductByServersServerIdAndProductId(int serverId, int productId);
    List<Product> findProductsByUsersUserId(int userId);
    Product findByProductName(String productName);
    Product findProductByBaseUrl(String baseUrl);

}