package com.a10networks.saas.products.util;

/**
 * @author SSingh
 * @email ssingh@a10networks.com
 */

public class ProductConstant {

	public static final String SEPARATOR = "/";
	public static final String API = "api";
	public static final String PRODUCT = "products";
	public static final String VERSION_V1 = "v1";
	public static final String END_POINT_API_V1 = SEPARATOR + API + SEPARATOR + VERSION_V1;
	public static final String END_POINT_PRODUCTS = SEPARATOR + PRODUCT;
	public static final String END_POINT_PRODUCTBYID = "/{id}";
	public static final String NOT_FOUND = "NOT_FOUND";
	public static final String SERVER_ERROR = "SERVER_ERROR";
	public static final String NO_DATA_FOUND_DESC = "No data found for requested Id.";
	public static final String FAILED_TO_PROCESS_DESC =  "Unable to process the request, please try later.";
	public static final String END_POINT_TENANTS = "/tenants";
	public static final String END_POINT_TENANTBYPRODUCTID = "/{tenantId}"+ SEPARATOR + PRODUCT;

	public static final String END_POINT_BASEURL = SEPARATOR + "baseUrl";
	public static final String PATHVARIABLE_BASEURL = "/{baseUrl}";
	public static final String SERVER = "server";
	public static final String END_POINT_SERVER = SEPARATOR + SERVER;
	public static final String END_POINT_SERVER_BY_PRODUCTS = "/{server_id}" + SEPARATOR + PRODUCT;
	public static final String END_POINT_PRODUCT_TO_SERVER = "/{server_id}" + SEPARATOR + PRODUCT + "/{product_id}";

	public static final String INVALID_COLUMN = "Invalid Column";
	public static final String INVALID_ID = "INVALID_ID";
	public static final String INVALID_COLUMN_DESC = "Column does not exists in table ";
	public static final String PRODUCT_ALREADY_EXIST_DESC = "Product already exist";
	public static final String ALREADY_EXIST = "DUPLICATE_ENTRY";

	public static final String END_POINT_USERS = "/users";
	public static final String END_POINT_USERBYPRODUCTID = "/{user_id}"+ SEPARATOR + PRODUCT;

	public static final String BAD_REQUEST = "BAD_REQUEST";
	public static final String UNPROCESSABLE_ENTITY = "UNPROCESSABLE_ENTITY";

	public static final String EMPTY_TABLE = "Empty Table";

	public static final String EMPTY_TABLE_DESC = "The Table does not contain any fields";
	public static final String DUPLICATE_ENTRY = "DUPLICATE_ENTRY";
}
