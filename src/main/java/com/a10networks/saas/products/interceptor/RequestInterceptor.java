package com.a10networks.saas.products.interceptor;

import com.a10.saas.kafka.KafkaAuditLibProducer;
import com.a10.saas.kafka.exception.AuditLibProducerException;
import com.a10.saas.kafka.util.TenantContext;
import com.a10networks.saas.products.exception.ProductTenantServiceException;
import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class RequestInterceptor implements HandlerInterceptor, WebMvcConfigurer {
    @Autowired
    private Environment env;

//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new LoggingInterceptor(env));
//    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object object) throws Exception {

//        TenantContext.setCurrentTenant(request);
//        KafkaAuditLibProducer kafkaAuditLibProducer = new KafkaAuditLibProducer();
//        try {
//            kafkaAuditLibProducer.initAuditTrail(request.getRequestURI());
//        } catch (AuditLibProducerException e) {
//            log.error(TenantConstant.FAILED_TO_PROCESS_DESC, e);
//            if(e.getStatusCode()==400)
//                throw new RecordNotFoundException(e.getErrorCode(), e.getMessage());
//            throw new TenantServiceException(e.getErrorCode(), e.getMessage());
//        }

        // observability
//        logInitializer.initLogging(request);
        return true;
    }

    @Override
    public void postHandle(
            HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
            throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object object, Exception ex) throws Exception {


//        logInitializer.endLogging();
    }
}
