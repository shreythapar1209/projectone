package com.a10networks.saas.products.controller;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.service.ProductService;
import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ProductConstant.END_POINT_API_V1 + ProductConstant.END_POINT_USERS)
public class ProductUserController {

    @Autowired
    private ProductService productService;

    @GetMapping(ProductConstant.END_POINT_USERBYPRODUCTID)
    @ResponseStatus(value = HttpStatus.OK)
    public ResponseEntity<List<Product>> getProductsByUserId(@PathVariable(value = "user_id") Integer userId) {
        List<Product> users = productService.getProductsByUserId(userId);
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

}

