package com.a10networks.saas.products.controller;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.service.ProductService;
import com.a10networks.saas.products.service.ProductServerService;
import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * An Product resource has precise control over Product.
 * <p>
 * This Product controller api resource is a member of the SaaS Framework.
 * </p>
 *
 *
 * @author NGupta3
 * @email NGupta3@a10networks.com
 * @see ProductService
 * @see ProductConstant
 * @see
 * * @since 1.0
 */

@RestController
@RequestMapping(ProductConstant.END_POINT_API_V1 + ProductConstant.END_POINT_SERVER)
public class ProductServerController {
	@Autowired
	private ProductServerService productServerService;

	@GetMapping(ProductConstant.END_POINT_SERVER_BY_PRODUCTS)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<Product>> getProductsByServerId(@PathVariable(value = "server_id") int serverId) {
		List<Product> Products = productServerService.getProductsByServerId(serverId);
		return new ResponseEntity<>(Products, HttpStatus.OK);
	}

	@GetMapping(ProductConstant.END_POINT_PRODUCT_TO_SERVER)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<Product> getProductByServerAndProductId(@PathVariable(value = "server_id") int serverId, @PathVariable(value = "product_id") int productId) {
		Product Product = productServerService.getProductByServerAndProductId(serverId,productId);
		return new ResponseEntity<>(Product, HttpStatus.OK);
	}

	@PutMapping(ProductConstant.END_POINT_PRODUCT_TO_SERVER)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void addProductServerMapping(@PathVariable(value = "server_id") int serverId, @PathVariable(value = "product_id") int productId) {
		productServerService.addProductServerMapping(serverId,productId);
	}

	@DeleteMapping(ProductConstant.END_POINT_PRODUCT_TO_SERVER)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delProductsServerMapping(@PathVariable(value = "server_id") int serverId, @PathVariable(value = "product_id") int productId) {
		productServerService.delProductsServerMapping(serverId,productId);
	}
}