package com.a10networks.saas.products.controller;

import com.a10networks.saas.products.entity.Product;
import com.a10networks.saas.products.service.ProductService;
import com.a10networks.saas.products.util.ProductConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * An tenant resource has precise control over tenant.
 * <p>
 * This tenant controller api resource is a member of the SaaS Framework.
 * </p>
 *
 *
 * @author NGupta3
 * @email NGupta3@a10networks.com
 * @see ProductService
 * @see ProductConstant
 * @see
 * * @since 1.0
 */

@RestController
@RequestMapping(ProductConstant.END_POINT_API_V1 + ProductConstant.END_POINT_TENANTS)
public class ProductTenantController {
	@Autowired
	private ProductService productService;

	@GetMapping(ProductConstant.END_POINT_TENANTBYPRODUCTID)
	@ResponseStatus(value = HttpStatus.OK)
	public ResponseEntity<List<Product>> getProductsByTenantId(@PathVariable(value = "tenantId") int tenantId) {
		List<Product> tenants = productService.getProductsByTenantId(tenantId);
		return new ResponseEntity<>(tenants, HttpStatus.OK);
	}
}