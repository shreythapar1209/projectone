package com.a10networks.saas.products.controller;

import java.util.List;
import java.util.Map;

import com.a10networks.saas.products.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import com.a10networks.saas.products.exception.ProductServiceException;
import com.a10networks.saas.products.service.ProductService;
import com.a10networks.saas.products.util.ProductConstant;

import javax.validation.Valid;

/**
 * An product resource has precise control over product management.
 * <p>
 * This product controller api resource is a member of the SaaS Framework.
 * </p>
 *
 * @author SSingh
 * @email ssingh2@a10networks.com
 * @see ProductService
 * @see ProductConstant
 * @see ProductServiceException
 * @since 1.0
 */

@RestController
@RequestMapping(ProductConstant.END_POINT_API_V1 + ProductConstant.END_POINT_PRODUCTS)
public class ProductController {

	@Autowired
	private ProductService productService;

	/**
	 * Fetch all products.
	 *
	 * @return List<{@link ProductEntity}> all products
	 * @throws {@link ProductServiceException} if the request can not be processed due
	 *                to any technical reason.
	 * @since  v1.0
	 * 
	 */
	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	List<Product> list(@RequestParam (required = false) String productName) throws ProductServiceException {
		return productService.findAll(productName);
	}

	/**
	 * Returns all new products record created.
	 * 
	 * @param products records to be created
	 * @return List<{@link ProductEntity}> products after creation
	 * @throws {@link IllegalArgumentException} if the request contains invalid
	 *                value or does not contain mandatory fields.
	 * @throws {@link ProductServiceException} if the request can not be processed due
	 *                to any technical reason.
	 * @since v1.0
	 * 
	 */
	@PostMapping
	@ResponseStatus(value = HttpStatus.CREATED)
	Product create(@Valid @RequestBody final Product products) throws ProductServiceException {
		return productService.create(products);
  	}

	/**
	 * Returns individual product record.
	 *
	 * @param id {@link Long} type system identification of product
	 * @return {@link Product}
	 * @throws {@link IllegalArgumentException} if the request contains invalid
	 *                value or does not contain mandatory fields.
	 * @throws {@link ProductServiceException} if the request can not be processed due
	 *                to any technical reason.
	 * @since v1.0
	 * 
	 */
	@GetMapping(ProductConstant.END_POINT_PRODUCTBYID)
	@ResponseStatus(value = HttpStatus.OK)
    Product single(@PathVariable final int id) throws ProductServiceException {
		return productService.single(id);
	}

	/**
	 * Returns modified product record.
	 * 
	 * @param product {@link Product} to be modified
	 * @param id   {@link Long} type system identification of product whose record to
	 *             be modified
	 * @return  product latest record after creation
	 * @throws {@link IllegalArgumentException} if the request contains invalid
	 *                value or does not contain mandatory fields.
	 * @throws {@link ProductServiceException} if the request can not be processed due
	 *                to any technical reason.
	 * @since v1.0
	 * 
	 */
	@PutMapping(ProductConstant.END_POINT_PRODUCTBYID)
	@ResponseStatus(value = HttpStatus.OK)
    Product modify(@Valid @RequestBody final Product product, @PathVariable final int id) throws ProductServiceException {
		return productService.modify(product, id);
	}

	/**
	 * Remove record from system.
	 * 
	 * @param  {@link Long} type system identification of product whose record to be
	 *           deleted.
	 * @throws {@link IllegalArgumentException} if the request contains invalid
	 *                value or does not contain mandatory fields.
	 * @throws {@link ProductServiceException} if the request can not be processed due
	 *                to any technical reason.
	 * @since v1.0
	 * 
	 */
	@DeleteMapping(ProductConstant.END_POINT_PRODUCTBYID)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	void remove(@PathVariable final int id) throws ProductServiceException {
		productService.remove(id);
	}

	@PatchMapping(ProductConstant.END_POINT_PRODUCTBYID)
	@ResponseStatus(value = HttpStatus.OK)
	Product updatePartially(@PathVariable int id, @RequestBody Map<String,Object> fields) throws ProductServiceException {
		return productService.updatePartially(id, fields);
	}

	@GetMapping(ProductConstant.END_POINT_BASEURL + ProductConstant.PATHVARIABLE_BASEURL)
	@ResponseStatus(value = HttpStatus.OK)
	Product getProductByBaseUrl(@PathVariable String baseUrl){
		return productService.getProductByBaseUrl(baseUrl);
	}
}